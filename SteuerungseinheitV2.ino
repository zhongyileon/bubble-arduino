// Libraries
#include <stdlib.h>
#include <SerialCmd.h>

// Variables and Objects
SerialCmd SCmd (Serial, SERIALCMD_CR, SERIALCMD_COMMA);

int camPin = 12;
int setPointPin = 3;
int valvePin = 8;
int voltageReadPin = A3;
int state = 0;
int command = 0;

long bubbletime = 0;
long pausetime = 0;
long bubblecaminterval = 0;
long pausecaminterval = 0;
long pausecamduration = 0;
bool timeSet = false;
int triggerTime = 30;


// Functions
void bubble_cycle(long bubbletime, long pausetime,
                  long bubblecaminterval, long pausecaminterval, long pausecamduration);
void triggerCam();
void startFlow(bool b);
void setTimes();
void transitionState();
void stateMachine();

enum State
{
  default_state,
  setTimes_state,
  measureFlow_state,
  bubble_state,
  pause_state,
};

State currentState = default_state;


// Serial commands
void manual();
void set_times();
void current();
void ask_times();



// MAIN CODE
void setup()
{
  // Open serial monitor 
  Serial.begin(9600);
  while (Serial != true) {}
  Serial.println("Serial communication initialized.");
  Serial.println("Type 'manual' into serial monitor to open instructions. Make sure to have the line ending set to carriage return (CR).");


  // Setting pins
  pinMode(setPointPin, OUTPUT);
  pinMode(camPin, OUTPUT);
  pinMode(valvePin, OUTPUT);
  pinMode(voltageReadPin, INPUT);


  // Serial Commands
  SCmd.AddCmd("set_times", SERIALCMD_FROMALL, set_times);
  SCmd.AddCmd("manual", SERIALCMD_FROMALL, manual);
  SCmd.AddCmd("current", SERIALCMD_FROMALL, current);
  SCmd.AddCmd("ask_times", SERIALCMD_FROMALL, ask_times);

}

void loop() 
{
  transitionState();
  stateMachine();
}


// FUNCTION DEFINITIONS

void transitionState()
{
  if (Serial.available() > 0 && Serial.available() < 3)
  {
    //Serial.println(Serial.available());
    char command = Serial.read();
    //Serial.print("command = ");
    //Serial.println(command);
    while (Serial.available() > 0) {Serial.read();} // empties buffer from various line endings
    
    //Serial.println("kakapupu");
    switch (command)
    {
      case '0':
        currentState = default_state;
        Serial.print("Switching to state ");
        Serial.println(currentState);
      break;

      case '1':
        currentState = setTimes_state;
        Serial.print("Switching to state ");
        Serial.println(currentState);
        timeSet = false;
      break;

      case '2':
        currentState = measureFlow_state;
        Serial.print("Switching to state ");
        Serial.println(currentState);
      break;

      case '3':
        currentState = bubble_state;
        Serial.print("Switching to state ");
        Serial.println(currentState);
      break;

      case '4':
        currentState = pause_state;
        Serial.print("Switching to state ");
        Serial.println(currentState);
      break;
    }
  }
  else if (Serial.available() > 2)
  {
    SCmd.ReadSer();
    //Serial.println("scmd path chosen");
  }
  
}

void stateMachine()
{

  switch(currentState)
  {
    case default_state: //default
      
      
      // Ensuring that camera is not taking pictures in default
      digitalWrite(camPin, LOW);

      // Ensuring that valve is initially open
      digitalWrite(valvePin, HIGH);

      // Ensuring that there is no flow
      startFlow(false);
    break;

    case setTimes_state: // set times
      if (timeSet == false)
      {
        //Serial.println("hi");
        setTimes();
      }
      timeSet = true;
    break;

    case measureFlow_state:
   
      startFlow(true);
      // Wait for input to start  
      while (Serial.available() == 0)
      {
        Serial.print(analogRead(voltageReadPin) * (100.0 / 1023.0));
        Serial.println(" mL/min");
        delay(1000);
      }
    break;

    case bubble_state:
    
      startFlow(true);
      bubble_cycle(bubbletime, pausetime, bubblecaminterval, pausecaminterval, pausecamduration);
    break;

    case pause_state:
     
      startFlow(false);
      digitalWrite(valvePin, HIGH);
      digitalWrite(camPin, LOW);

    break;
  }
}

void setTimes()
{
  if (Serial.available() > 0)
  {
    Serial.read();
  }

  Serial.println("Set bubble time in milliseconds: ");
  while (Serial.available() == 0) {}
  bubbletime = Serial.parseInt();
  while (Serial.available() > 0) {Serial.read();} // this line ensures that the program works with line endings
  if (bubbletime < 0)
  {
    bubbletime = 0;
  }
  Serial.print("Bubbletime set to ");
  Serial.println(bubbletime);

  Serial.println("Set pause time in milliseconds: ");
  while (Serial.available() == 0) {}
  pausetime = Serial.parseInt();
  while(Serial.available() > 0) {Serial.read();}
  if (pausetime < 0)
  {
    pausetime = 0;
  }
  Serial.print("Pause time set to ");
  Serial.println(pausetime);

  Serial.println("Set interval between camera triggers while bubbling: ");
  while (Serial.available() == 0) {}
  bubblecaminterval = Serial.parseInt();
  while(Serial.available() > 0) {Serial.read();}
  if (bubblecaminterval < 0)
  {
    bubblecaminterval = 0;
  }
  Serial.print("Cam trigger interval while bubbling set to ");
  Serial.println(bubblecaminterval);

  Serial.println("Set interval between camera triggers while not bubbling: ");
  while (Serial.available() == 0) {}
  pausecaminterval = Serial.parseInt();
  while(Serial.available() > 0) {Serial.read();}
  if (pausecaminterval < 0)
  {
    pausecaminterval = 0;
  }
  Serial.print("Cam trigger interval while not bubbling set to ");
  Serial.println(pausecaminterval);

  Serial.println("Set duration in which camera triggers while not bubbling (shoudl be less than pause time): ");
  while (Serial.available() == 0) {}
  pausecamduration = Serial.parseInt();
  while(Serial.available() > 0) {Serial.read();}
  if (pausecamduration < 0)
  {
    pausecamduration = 0;
  }
  if (pausecamduration > pausetime)
  {
    pausecamduration = pausetime;
  }
  Serial.print("Cam duration while not bubbling set to ");
  Serial.println(pausecamduration);
}

void triggerCam()
{
  digitalWrite(camPin, HIGH); // this triggers the cam
  delay(triggerTime);
  digitalWrite(camPin, LOW); // back to not triggered
}

void startFlow(bool b) //from 0 until 255
{
  if (b == true)
  {
    digitalWrite(setPointPin, HIGH);
  }
  else
  {
    digitalWrite(setPointPin, LOW);
  }
}


// blubberzeit, pausenzeit, bubblecaminterval (Zeitabstand in der die Kamera während bubblezeit triggert), pausecaminterval,
// pausdecamduration (gibt an in welcher zeitspanne in pausetime die kamera triggern soll)
void bubble_cycle(long bubbletime, long pausetime,
                  long bubblecaminterval, long pausecaminterval, long pausecamduration)
{
  // close valve for bubbling
  digitalWrite(valvePin, LOW);

  // wait for the given bubble time
  unsigned long startTime = millis();
  unsigned long currentTime = millis();
  while (currentTime - startTime <= bubbletime)
  {
    triggerCam();
    delay(bubblecaminterval);

    currentTime = millis(); 
  }

  // stop bubbling by opening valve
  digitalWrite(valvePin, HIGH);
  
  // wait for given pause time  
    // to do: check that pause cam duration <= pausetime  
  startTime = millis();
  currentTime = millis();

  unsigned long innerStartTime = millis();
  unsigned long innerCurrentTime = millis();

  while (currentTime - startTime <= pausetime)
  {
    
    while (innerCurrentTime - innerStartTime <= pausecamduration)
    {
      triggerCam();
      delay(pausecaminterval);
      innerCurrentTime = millis();
    }

    currentTime = millis(); 
  }
}


// SERIAL COMMANDS DEFINITION

void manual()
{
  Serial.println();
  Serial.println(F("MANUAL: THIS IS THE CONTROL UNIT FOR THE BUBBLING SETUP!"));
  Serial.println(F("Via the Serial Monitor you can switch between five states by typing in the respective state number. Those are:"));
  Serial.println();

  Serial.println(F("State 0: DEFAULT"));
  Serial.println(F("In this default state, the flow is 0, the camera doesn't trigger and the magnetic valve is open."));
  Serial.println();

  Serial.println(F("State 1: SET TIMES"));
  Serial.println(F("In this state, you are asked to set the times for bubbling and pausing, as well as the camera trigger intervals."));
  Serial.println(F("Here the gas flow is not yet opened and the camera is still not triggering."));
  Serial.println();

  Serial.println(F("State 2: SET FLOWRATE"));
  Serial.println(F("In this state, the gas flow is turned on. You can adjust the flow rate on the potentiometer while the serial monitor prints out the current flow."));
  Serial.println();

  Serial.println(F("State 3: BUBBLE CYCLE"));
  Serial.println(F("This state is the actual bubbling/pausing cycle in which the camera takes pictures. It will go on indefinitely until you switch into another state."));
  Serial.println(F("Switching into another state will happen after the current bubble cycle is completed."));
  Serial.println();

  Serial.println(F("State 4: PAUSE"));
  Serial.println(F("This state is identical to the default state and has literally no use hahahaha but you can still use it if you want :)"));
  Serial.println();

  Serial.println(F("OTHER USEFUL COMMANDS: Just type them into the serial monitor"));
  Serial.println(F("'current': current displays the current state you are in."));
  Serial.println(F("'ask_times': ask_times displays the five different times you have set the system to."));
  Serial.println(F("'manual': manual opens up the manual again."));
  Serial.println(F("'set_times,bubbletime*,pausetime*,bubblecaminterval*,pausecaminterval*,pausecamduration*': This allows"));
  Serial.println(F("you to change the times without switching into State 2. Replace the parameters marked by * with the "));
  Serial.println(F("desired times in ms."));
}

void set_times()
{
  char* time;

  time = SCmd.ReadNext();
  bubbletime = strtoul(time, NULL, 10);
  if (bubbletime < 0)
  {
    bubbletime = 0;
  }

  time = SCmd.ReadNext();
  pausetime = strtoul(time, NULL, 10);
  if (pausetime < 0)
  {
    pausetime = 0;
  }


  time = SCmd.ReadNext();
  bubblecaminterval = strtoul(time, NULL, 10);
  if (bubblecaminterval < 0)
  {
    bubblecaminterval = 0;
  }

  time = SCmd.ReadNext();
  pausecaminterval = strtoul(time, NULL, 10);
  if (pausecaminterval < 0)
  {
    pausecaminterval = 0;
  }

  time = SCmd.ReadNext();
  pausecamduration = strtoul(time, NULL, 10);
  if (pausecamduration < 0)
  {
    pausecamduration = 0;
  }
  if (pausecamduration > pausetime)
  {
    pausecamduration = pausetime;
  }

  Serial.println("Times set.");

}

void current()
{
  Serial.print(F("Currently in state "));
  Serial.println(currentState);
}

void ask_times()
{
  Serial.print(F("Bubble time set to: "));
  Serial.print(bubbletime);
  Serial.println(F("ms."));

  Serial.print(F("Pause time set to: "));
  Serial.print(pausetime);
  Serial.println(F("ms."));

  Serial.print(F("Camera trigger interval while bubbling set to: "));
  Serial.print(bubblecaminterval);
  Serial.println(F("ms."));

  Serial.print(F("Camera trigger interval while not bubbling set to: "));
  Serial.print(pausecaminterval);
  Serial.println(F("ms."));

  Serial.print(F("Camera trigger period while not bubbling set to: "));
  Serial.print(pausecamduration);
  Serial.println(F("ms."));
}